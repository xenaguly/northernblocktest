import Joi from 'joi';

export default {
  // POST /api/users
  createUser: {
    body: {
      username: Joi.string().required(),
      mobileNumber: Joi.string().regex(/^[1-9][0-9]{9}$/).required()
    }
  },
  // POST /api/posts
  createPost: {
    body: {
      title: Joi.string().required(),
    }
  },

	// POST /api/dogs
  createDog: {
    body: {
      name: Joi.string().required(),
	  breed: Joi.string().required(),
	  sex: Joi.string().regex(/^M$|^m$|^F$|^f$/).required(),
	  weight: Joi.number().positive().required(),
      locate: {
		  latitude: Joi.number().min(-90).max(90).required(),
		  longitude: Joi.number().min(-180).max(180).required()
	  }
    }
  },  

  // UPDATE /api/users/:userId
  updateUser: {
    body: {
      username: Joi.string().required(),
      mobileNumber: Joi.string().regex(/^[1-9][0-9]{9}$/).required()
    },
    params: {
      userId: Joi.string().hex().required()
    }
  },
  
  // UPDATE /api/dogs/:dogId
  updateDog: {
    body: {
      name: Joi.string().required(),
	  breed: Joi.string().required(),
	  sex: Joi.string().regex(/^M$|^m$|^F$|^f$/).required(),
	  weight: Joi.number().positive().required(),
      locate: {
		  latitude: Joi.number().min(-90).max(90).required(),
		  longitude: Joi.number().min(-180).max(180).required()
	  }
    },
	params: {
      dogId: Joi.string().hex().required()
    }
  }, 

  // UPDATE /api/posts/:postId
  updatePost: {
    body: {
      title: Joi.string().required(),
    },
    params: {
      postId: Joi.string().hex().required()
    }
  },

  // POST /api/auth/login
  login: {
    body: {
      username: Joi.string().required(),
      password: Joi.string().required()
    }
  }
};

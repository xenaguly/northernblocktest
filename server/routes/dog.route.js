import express from 'express';
import validate from 'express-validation';
import paramValidation from '../config/param-validation';
import dogCtrl from '../controllers/dog.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/dogs - Get list of dogs */
  .get(dogCtrl.list)

  /** POST /api/dogs - Create new dogs */
  .post(validate(paramValidation.createDog), dogCtrl.create);

router.route('/:dogId')
  /** GET /api/dogs/:dogId - Get dogs */
  .get(dogCtrl.get)

  /** PUT /api/dogs/:dogId - Update dogs */
  .put(validate(paramValidation.updateDog), dogCtrl.update)

  /** DELETE /api/dogs/:dogId - Delete dogs */
  .delete(dogCtrl.remove);
  
router.route('/:dogId/image')
	.get(dogCtrl.getImage);

/** Load dogs when API with dogId route parameter is hit */
router.param('dogId', dogCtrl.load);

export default router;

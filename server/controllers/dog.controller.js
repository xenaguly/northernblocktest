import Dog from '../models/Dog.model';
import APIError from '../helpers/APIError';

const download = require('image-downloader');
const request = require('request');
const path = require('path');
const fs = require('fs');


/**
 * Load Dog and append to req.
 */
function load(req, res, next, id) {
  Dog.get(id)
    .then((dog) => {
      req.dog = dog; // eslint-disable-line no-param-reassign
      return next();
    })
    .catch(e => next(e));
}

/**
 * Get Dog
 * @returns {Dog}
 */
function get(req, res) {
  return res.json(req.dog);
}




/**
 * I am not sure if you want the dog to have the same picture in future fetching, 
 * so I assume the picture can be different every time there is a request. If you
 * want the dog to have the same picture in future fetching, the way to do so is
 * to put the image name into the Dog object after the first fetch, and get that
 * picture in any future fetch for that dog
 */
function getImage(req, res, next) {
	const dog = req.dog;
	if (!dog) {
		res.set('Content-Type', 'text/plain');
		res.status(404).end('Not found');
		return;
	}
	const url = 'https://dog.ceo/api/breed/'+dog.breed+'/images/random';
	
	// get the url of the pic from breed
	return request(url, function (error, response, body) {
		if (error) {
			res.set('Content-Type', 'text/plain');
			res.status(404).end('Dog Image is not found');
			return;
		}
		
		const b = JSON.parse(body);
		
		const options = {
		  url: b.message,
		  dest: './server/dog_pic'                 
		}
		
		// download the image
		return download.image(options)
		  .then(({ filename, image }) => {
			var mime = {
				html: 'text/html',
				txt: 'text/plain',
				css: 'text/css',
				gif: 'image/gif',
				jpg: 'image/jpeg',
				png: 'image/png',
				svg: 'image/svg+xml',
				js: 'application/javascript'
			};
			var type = mime[path.extname(filename).slice(1)] || 'text/plain';
			
			// serve the image
			var s = fs.createReadStream(filename);
			s.on('open', function () {
				res.set('Content-Type', type);
				s.pipe(res);
			});
			s.on('error', function () {
				res.set('Content-Type', 'text/plain');
				res.status(404).end('Dog Image is not found');
			});
			return s;
			
		  })
		  .catch((err) => {
			  console.log(err);
			res.status(404).end('Dog Image is not found');
		  });
	});
}

/**
 * Create new Dog
 * @property {string} req.body.username - The username of Dog.
 * @property {string} req.body.mobileNumber - The mobileNumber of Dog.
 * @returns {Dog}
 */
function create(req, res, next) {
  const dog = new Dog({
    name: req.body.name,
    breed: req.body.breed,
	sex: req.body.sex,
	weight: req.body.weight,
	locate: {
		latitude: req.body.locate.latitude,
		longitude: req.body.locate.longitude
	}
  });

  dog.save()
    .then(savedDog => res.json(savedDog))
    .catch(e => next(e));
}

/**
 * Update existing Dog
 * @property {string} req.body.username - The username of Dog.
 * @property {string} req.body.mobileNumber - The mobileNumber of Dog.
 * @returns {Dog}
 */
function update(req, res, next) {
  const dog = req.dog;
  dog.name = req.body.name;
  dog.breed = req.body.breed;
  dog.sex = req.body.sex;
  dog.weight = req.body.weight;
  dog.locate = req.body.locate;

  dog.save()
    .then(savedDog => res.json(savedDog))
    .catch(e => next(e));
}

/**
 * Get Dog list.
 * @property {number} req.query.skip - Number of dogs to be skipped.
 * @property {number} req.query.limit - Limit number of dogs to be returned.
 * @returns {Dog[]}
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Dog.list({ limit, skip })
    .then(dogs => res.json(dogs))
    .catch(e => next(e));
}

/**
 * Delete Dog.
 * @returns {Dog}
 */
function remove(req, res, next) {
  const dog = req.dog;
  dog.remove()
    .then(deletedDog => res.json(deletedDog))
    .catch(e => next(e));
}

export default { load, get, create, update, list, remove , getImage};
